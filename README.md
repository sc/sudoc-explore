# SUDOC, yazet z3950

Suite d'outils pour explorer SUDOC via z3950. La document complete deschamps z3950  sur SUDO est diponible [ici](https://www.transition-bibliographique.fr/unimarc/manuel-unimarc-format-bibliographique/)

Le but du projt et d'effectuer des requete poru recuperer les contenu de bibliothèque spécialiser de ces bibliothèques via  le numero RCR de ces dites bibliothèques.
Chaque fiche de chaque document present a la bibliotheque est ensuite sauvegarder ; ecertian element sont ensuite recuperer et stocké sous forme de cvs.

Pour envoyer les requets on utilse [yaz-client](https://software.indexdata.com/yaz/)

Ya un paquet debian donc un exemple mini est:

```
$ apt install yaz 
$ yaz-client
Z> open carmin.sudoc.abes.fr/ABES-Z39-PUBLIC
Z> find @attr 1=56 "341725201"
```



# Contenu du dossier:


- `allrcrs/`: pour chaque code RCR le resultat fourni par la reque z3950 show all
- `countNgroup.sh`: ptit scirp naz pour content le nombre de fiche total
- `download_all.sh`: script qui fait tout les requetes en fonction d'un fichier text avec la list des RCR  
- `extracteur.py`: script qui extrait les different champs pour creer un pdf
- `info/`: differents infor utilsé par les scripts précédents
    + `info/Numéro unimarc à conserver pour base patrimaths.csv`: numero unimarc a conserver dans le pdf
    + `info/rcr.txt`: list de numero RCR a interroger

En dessous plus de détails sur les deux scripts principaux: 

## download_all.sh

`download_all.sh` est un script shell qui télécharge des informations à partir d'un serveur Z39.50 (carmin.sudoc.abes.fr) pour chaque RCR (Répertoire des Centres de Ressources) listé dans le fichier d'entrée fourni en tant que premier argument du script. Les résultats sont enregistrés dans des fichiers distincts avec un préfixe `resall_` dans le dossier spécifié en tant que deuxième argument du script.

Le script effectue les opérations suivantes :

1. Définit les chemins d'entrée et de sortie, l'adresse du serveur et le temps de pause.
2. Crée un dossier de sortie s'il n'existe pas déjà.
3. Parcourt chaque RCR dans le fichier d'entrée.
4. Crée une chaîne de commandes pour le client YAZ.
5. Exécute les commandes avec le client YAZ et redirige la sortie vers un fichier.
6. Attend une seconde pour éviter de surcharger le serveur.

### Exemple d'utilisation

Pour utiliser ce script, assurez-vous d'avoir un fichier contenant la liste des RCR (par exemple, `rcr_list.txt`). Créez un dossier pour enregistrer les fichiers de sortie (par exemple, `output_folder/`).

Ensuite, exécutez le script `download_all.sh` avec les arguments appropriés dans un terminal :

```sh
bash ./download_all.sh rcr_list.txt output_folder
```

Le script téléchargera les informations pour chaque RCR dans le fichier `rcr_list.txt` et les enregistrera dans des fichiers distincts dans le dossier `output_folder/`.

## extracteur.py

`extracteur.py` est un script Python qui extrait des données spécifiques à partir de fichiers texte présents dans un dossier et les enregistre dans un fichier CSV. Ce script est conçu pour lire des fichiers contenant des données UNIMARC, puis extraire les champs spécifiés dans un fichier CSV de champs à extraire.

Le script effectue les opérations suivantes :

1. Lit les champs à extraire à partir du fichier de champs.
2. Extrait les données des fichiers texte.
3. Écrit les données extraites dans un fichier CSV.

### Exemple d'utilisation

Pour utiliser ce script, assurez-vous que les fichiers texte à traiter sont dans un dossier (par exemple, `testout/`). Créez un fichier CSV contenant les champs à extraire (par exemple, `info/Numéro unimarc à conserver pour base patrimaths.csv`).

Ensuite, exécutez le script `extracteur.py` via:
```bash
python3 extracteur.py
```
 Les données extraites seront enregistrées dans un fichier CSV de sortie (par exemple, `output.csv`).


