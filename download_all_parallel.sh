# Define input and output paths, server address, and sleep time
input_path_rcr="info/rcr.txt"
output_prefix="resall_"
server_address="carmin.sudoc.abes.fr/ABES-Z39-PUBLIC"
sleep_time=1
output_folder="$1"

if [ ! -d "$output_folder" ]; then
    mkdir -p "$output_folder"
fi

# Function to process each RCR
process_rcr() {
    rcr=$1
    echo "rcr = $rcr"

    # Initialize variables for the YAZ command
    record=1

    # Execute commands with the YAZ client and redirect the output to a file
    cmd="format unimarc
negcharset UTF-8
open $server_address
find @attr 1=56 '$rcr'
"
    # Execute the Yaz command and redirect the output to a temporary file
    echo "$cmd" | yaz-client -f /dev/stdin > tmp_$rcr.txt
    
    # Extract the resultset from the Yaz response to use in the next command
    nb_hits=`grep "Number of hits" tmp_$rcr.txt | awk -F': ' '{print $2}' | awk -F',' '{print $1}'`
    echo "nhits=$nb_hits"
    if [ $nb_hits -gt 0 ]; then
        while [ $record -ne 0 ]
        do
            cmd="format unimarc
    negcharset UTF-8
    open $server_address
    find @attr 1=56 '$rcr'
    show $record"
            # Execute the Yaz command and redirect the output to a temporary file
            echo "$cmd" | yaz-client -f /dev/stdin >tmp_$rcr.txt
            cat tmp_${rcr}.txt >> ${output_folder}/${output_prefix}${rcr}.txt;

            # Update the record number for the next request
            nextResultSetPosition=$(grep -oP 'nextResultSetPosition = \K\d+' tmp_$rcr.txt) 
            record=$nextResultSetPosition
            echo "checking $record/$nb_hits - $rcr"
        done
        rm tmp_$rcr.txt
    fi
}

export -f process_rcr
export output_folder
export output_prefix
export server_address

cat $input_path_rcr | parallel -j 37 process_rcr


