# Définir les chemins d'entrée et de sortie, l'adresse du serveur et le temps de pause
input_path_rcr="info/rcr.txt"
input_path_hits="info/numbers_hits.txt"
output_prefix="resall_"
server_address="carmin.sudoc.abes.fr/ABES-Z39-PUBLIC"
sleep_time=1
output_folder="$1"

if [ ! -d "$output_folder" ]; then
    mkdir -p "$output_folder"
fi

# Boucle pour chaque RCR dans le fichier d'entrée
for rcr in `cat $input_path_rcr`; do
    # Définir le nombre de hits précis en prenant la i ligne du fichier numbers_hits
    echo "rcr = $rcr"

    # Initialiser les variables pour la commande YAZ
    record=1

    # Exécuter les commandes avec le client YAZ et rediriger la sortie vers un fichier
    # Créer une chaîne de commandes pour le client YAZ
    cmd="format unimarc
negcharset UTF-8
open $server_address
find @attr 1=56 '$rcr'
"
    # Exécuter la commande Yaz et rediriger la sortie vers un fichier temporaire
    echo "$cmd" | yaz-client -f /dev/stdin > tmp_$rcr.txt
    
    # Extraire le résultatset de la réponse Yaz pour l'utiliser dans la commande suivante
    nb_hits=`grep "Number of hits" tmp_$rcr.txt | awk -F': ' '{print $2}' | awk -F',' '{print $1}'`
    echo "nhits=$nb_hits"
    if [ $nb_hits -gt 0 ]; 
    then
        while [ $record -ne 0 ]
        do
            echo "checking  $record/$nb_hits"
            cmd="format unimarc
    negcharset UTF-8
    open $server_address
    find @attr 1=56 '$rcr'
    show $record"
            # Exécuter la commande Yaz et rediriger la sortie vers un fichier temporaire
            echo "$cmd" | yaz-client -f /dev/stdin >tmp_$rcr.txt
            cat tmp_${rcr}.txt >> ${output_folder}/${output_prefix}${rcr}.txt;

            # Mettre à jour la position de départ pour la prochaine requête
            nextResultSetPosition=$(grep -oP 'nextResultSetPosition = \K\d+' tmp_$rcr.txt) 
            record=$nextResultSetPosition
        done
        rm tmp_$rcr.txt
    fi

    # Supprimer le fichier temporaire
done
