import csv
import os
import re

input_folder = "resparal/" # Dossier contenant les fichiers à lire
output_file = "newoutput.csv" # Nom du fichier CSV de sortie
fields_file = "info/Numéro unimarc à conserver pour base patrimaths.csv" # Nom du fichier contenant les champs à extraire

# Fonction pour lire les champs à extraire à partir du fichier de champs
def read_fields(fields_file: str):
    fields = {}
    with open(fields_file, 'r', encoding='utf-8') as file:
        csv_reader = csv.reader(file, delimiter=';')
        next(csv_reader)  # Ignorer la ligne d'en-tête
        for row in csv_reader:
            # Supprimer les espaces vides au début et à la fin de chaque élément
            row = [element.strip() for element in row]
            # Vérifier si la ligne n'est pas vide et contient une clé et une valeur
            if len(row) >= 2 and row[0] and row[2]:
                key = str(f"{int(row[0]):03d}")  # Convertir la clé en une chaîne et ajouter des zéros au début
                fields[key] = '"'+row[2]+'"' # Stocker la valeur extraite dans le dictionnaire des champs
        fields['filename'] = 'filename' # Ajouter une clé pour stocker le nom du fichier
    return fields


# Fonction pour extraire les données des fichiers texte
def extract_data(input_folder,fields):
    data = [] # Initialiser la liste pour stocker les données extraites
    for filename in os.listdir(input_folder): # Itérer sur tous les fichiers dans le dossier d'entrée
        prev=len(data) # Afficher le nombre d'entrées extraites dans chaque fichier
        input_file = os.path.join(input_folder, filename) # Obtenir le chemin complet du fichier
        current_record = {} # Initialiser un dictionnaire pour stocker les données extraites de chaque fichier
        with open(input_file, 'r',encoding="utf-8",errors="ignore") as f: # Ouvrir le fichier en mode lecture
            for line in f:
                if line.strip() == "": # Si la ligne est vide, cela signifie qu'une nouvelle entrée commence
                    current_record['filename'] = filename # Ajouter le nom du fichier dans l'entrée courante
                    for field_code in fields.keys():
                        if field_code not in current_record.keys():
                            current_record[field_code]="" # Si un champ n'a pas été trouvé dans l'entrée courante, ajouter une valeur vide
                    data.append(current_record) # Ajouter l'entrée courante dans la liste des données extraites
                    current_record = {} # Initialiser un nouveau dictionnaire pour stocker les données extraites de la prochaine entrée
                for  field_code in fields.keys():
                    if line.startswith(field_code+" "):
                        # Extraire la valeur à partir de la ligne correspondante
                        value = re.split("\$. ",line)[1].strip()
                        current_record[field_code] = value # Ajouter la valeur extraite dans le dictionnaire des données extraites
            print(filename+":"+str(len(data)-prev)+",tot:"+str(len(data))) # Afficher le nombre d'entrées extraites dans chaque fichier
    return data



# Fonction pour écrire les données extraites dans un fichier CSV
def write_csv(data, output_file, fields):
    with open(output_file, 'w') as f: # Ouvrir le fichier de sortie en mode écriture
        # Écrire l'en-tête
        header = list(fields.values()) # Récupérer les noms des champs extraits
        kh = {v: k for k, v in fields.items()} # Créer un dictionnaire pour faire correspondre les noms des champs avec leurs clés
        f.write(','.join(header) + '\n') # Écrire l'en-tête dans le fichier de sortie
        # Écrire les données
        for record in data: # Itérer sur toutes les entrées extraites
            allvalues=[record[kh[field]] for field in header] # Récupérer les valeurs pour chaque champ extrait dans l'ordre de l'en-tête
            # Écrire chaque valeur dans le fichier de sortie, en encapsulant les valeurs de chaînes de caractères entre guillemets doubles
            f.write(",".join('"{0}"'.format(value.replace('"', '""')) if isinstance(value, str) else str(value) for value in allvalues) + "\n")


fields =  read_fields(fields_file)
data = extract_data(input_folder, fields)
write_csv(data, output_file, fields)
